package ru.pavlovka.homework5.collections;

import java.util.*;

public interface MyUtilCollections {
    static <T> void swap(List<T> list, int i, int j){
        if(i>=list.size() || j>=list.size()){
            return;
        }

        if(list instanceof RandomAccess){
            T valueI=list.get(i);
            T valueJ=list.get(j);
            list.set(i, valueJ);
            list.set(j, valueI);
        }else{
            T[] arr= (T[]) list.toArray();
            T valueI=arr[i];
            T valueJ=arr[j];
            arr[i]=valueJ;
            arr[j]=valueI;
            list.clear();
            for(T el: arr){
                list.add(el);
            }
        }
    }

    static int countAllIntegerValues(Map<String, Set<Integer>> map){
        int result=0;
        for(Map.Entry<String, Set<Integer>> entry: map.entrySet()){
            result+=entry.getValue().size();
        }
        return result;
    }
}
