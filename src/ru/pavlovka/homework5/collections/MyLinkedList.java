package ru.pavlovka.homework5.collections;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class MyLinkedList<E> implements ILinkedList<E> {

    private Node<E> head;

    private int size;

    public MyLinkedList(){
        head = new Node<E>();
    }

    @Override
    public void add(E element) {
        Node<E> lastNode=lastNode();
        lastNode.nextNode=new Node<E>(element);
        size++;
    }

    @Override
    public void add(int index, E element) {
        Node<E> node=getNode(index);
        Node<E> nextNode=node.nextNode;
        node.nextNode=new Node<>(element, nextNode);
        size++;
    }

    @Override
    public void clear() {
        Node<E> node=head;
        while(node.nextNode!=null){
            node.element=null;
            Node<E> prevNode=node;
            node=node.nextNode;
            prevNode.nextNode=null;
        }
        size=0;
    }

    @Override
    public E get(int index) {
        return getNode(index).element;
    }

    @Override
    public int indexOf(E element) {
        int index=-1;
        for(E e: this){
            index++;
            if(e.equals(element)){
                break;
            }
        }
        return index;
    }

    @Override
    public E remove(int index) {

        E element = null;

        Node<E> prevNode = getNode(index-1);
        Node<E> node=prevNode.nextNode;
        if(node!=null){
            element=node.element;
            prevNode.nextNode=node.nextNode;
            node.nextNode=null;
            node.element=null;
            size--;
        }
        return element;
    }

    @Override
    public E set(int index, E element) {
        Node<E> node=getNode(index);
        E prevElement=node.element;
        node.element=element;
        return prevElement;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public E[] toArray() {
        E[] arr = (E[]) new Object[size()];
        int i=0;
        for(E element: this){
            arr[i]=element;
            i++;
        }
        return arr;
    }

    @Override
    public String toString(){
        List<String> list=new LinkedList<>();
        for(E element: this){
            if(element==null){
                continue;
            }
            list.add(element.toString());
        }
        return this.getClass().getName()+"{"+String.join(", ", list)+"}";
    }

    @Override
    public Iterator<E> iterator() {
        return new Iterator<E>() {

            private Iterator<Node<E>> nodeIterator=nodeIterator();

            @Override
            public boolean hasNext() {
                return nodeIterator.hasNext();
            }

            @Override
            public E next() {
                return nodeIterator.next().element;
            }
        };
    }

    private Node<E> lastNode(){
        Iterator<Node<E>> nodeIterator=nodeIterator();
        Node<E> currentNode=head;
        while(nodeIterator.hasNext()){
            currentNode=nodeIterator.next();
        }
        return currentNode;
    }

    private Node<E> getNode(int index){
        int count=-1;
        Iterator<Node<E>> nodeIterator=nodeIterator();
        Node<E> currentNode=head;
        while (nodeIterator.hasNext() && count<index){
            currentNode=nodeIterator.next();
            count++;
        }
        return currentNode;
    }

    private Iterator<Node<E>> nodeIterator(){
        return new Iterator<Node<E>>() {

            Node<E> currentNode=head;

            @Override
            public boolean hasNext() {
                return currentNode.nextNode!=null;
            }

            @Override
            public Node<E> next() {
                if(hasNext()){
                    currentNode=currentNode.nextNode;
                };
                return currentNode;
            }
        };
    }

    private class Node<E>{

        private E element;

        private Node<E> nextNode;

        public Node(E element, Node<E> nextNode){
            this.element=element;
            this.nextNode=nextNode;
        }

        public Node(E element){
            this(element, null);
        }

        public Node(){
            this(null, null);
        }
    }
}
