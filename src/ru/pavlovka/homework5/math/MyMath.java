package ru.pavlovka.homework5.math;

import java.math.BigInteger;
import java.util.*;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public interface MyMath {
    static Set<Integer> sieveOfEratosphene(int n){
        Set<Integer> result=new HashSet<>();
        for(int i=2; i<=n; i++){
            result.add(i);
        }
        int p=2;
        while (p<=n){
            for(int i=2; i*p<=n; i++){
                result.remove(i*p);
            }
            int prevP=p;
            for(int i=p+1; i<=n; i++){
                if(result.contains(i)){
                    p=i;
                    break;
                }
            }
            if(prevP==p){
                break;
            }
        }
        return result;
    }

    static double avg(DoubleStream stream){
        return stream.summaryStatistics().getAverage();
    }
}
