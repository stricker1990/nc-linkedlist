package ru.pavlovka.homework5;

import ru.pavlovka.homework5.collections.ILinkedList;
import ru.pavlovka.homework5.collections.MyLinkedList;
import ru.pavlovka.homework5.collections.MyUtilCollections;
import ru.pavlovka.homework5.files.TextFileStats;
import ru.pavlovka.homework5.graphes.Graph;
import ru.pavlovka.homework5.graphes.Neighbor;
import ru.pavlovka.homework5.math.MyMath;
import ru.pavlovka.homework5.perfomance.PerfomanceMonitor;
import ru.pavlovka.homework5.util.Rand;

import java.util.*;
import java.util.stream.DoubleStream;
import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) {
        testMyLinkedList();
        compareLinkedLists();
        compareLists();
        compareSet();
        compareMap();

        testSieveEraposphene();
        testSwap();
        testEx7_6();
        testReadFiles();
        testDijkstra();
        testStream();
    }

    static void testMyLinkedList(){
        ILinkedList<Integer> list= new MyLinkedList();

        list.add(10);
        list.add(9);
        list.add(8);
        System.out.println(list);
        list.add(0, 10);
        System.out.println(list);
        list.add(list.size(), 11);
        System.out.println(list);
        list.add(2, 22);
        System.out.println(list);

        list.set(2, 33);
        System.out.println(list);

        System.out.println(list.indexOf(8));
        list.remove(2);

        System.out.println(Arrays.toString(list.toArray()));

        System.out.println(list);
        System.out.println("size: "+list.size());
        list.clear();

        System.out.println(list);
        System.out.println("size: "+list.size());

        for(int i=0; i<10; i++){
            list.add(i);
        }
        System.out.println(list.size());
        System.out.println(list);
    }

    public static void compareLinkedLists(){

        System.out.println("COMPARE LinkedLists");

        ILinkedList<String> myList=new MyLinkedList();
        List<String> list=new LinkedList<>();

        List<String> testData= Rand.getRandomStrings(10000);

        System.out.println("AVG ADD MyList: "+PerfomanceMonitor.getAveragePerfomence(testData, (str)->myList.add(str)));
        System.out.println("AVG ADD LinkedList: "+PerfomanceMonitor.getAveragePerfomence(testData, (str)->list.add(str)));

        System.out.println("AVG FIND MyList: "+PerfomanceMonitor.getAveragePerfomence(testData, (str)->myList.indexOf(str)));
        System.out.println("AVG FIND LinkedList: "+PerfomanceMonitor.getAveragePerfomence(testData, (str)->list.indexOf(str)));

        List<Integer> indexes=new ArrayList<>(10000);
        for(int i=0; i<testData.size(); i++){
            indexes.add(i);
        }

        System.out.println("AVG GET MyList: "+PerfomanceMonitor.getAveragePerfomence(indexes, (i)->myList.get(i)));
        System.out.println("AVG GET LinkedList: "+PerfomanceMonitor.getAveragePerfomence(indexes, (i)->list.get(i)));

        Collections.shuffle(indexes);

        System.out.println("AVG REMOVE MyList: "+PerfomanceMonitor.getAveragePerfomence(indexes, (i)->myList.remove(i)));
        System.out.println("AVG REMOVE LinkedList: "+PerfomanceMonitor.getAveragePerfomence(indexes, (i)->list.remove(i)));

    }

    public static void compareLists(){
        System.out.println("COMPARE Lists");

        List<String> testData= Rand.getRandomStrings(10000);

        List<String> arrayList=new ArrayList<>(0);
        List<String> linkedList=new LinkedList<>();

        System.out.println("AVG ADD ArrayList: "+PerfomanceMonitor.getAveragePerfomence(testData, (str)->arrayList.add(str)));
        System.out.println("AVG ADD LinkedList: "+PerfomanceMonitor.getAveragePerfomence(testData, (str)->linkedList.add(str)));

        System.out.println("AVG FIND ArrayList: "+PerfomanceMonitor.getAveragePerfomence(testData, (str)->arrayList.indexOf(str)));
        System.out.println("AVG FIND LinkedList: "+PerfomanceMonitor.getAveragePerfomence(testData, (str)->linkedList.indexOf(str)));

        System.out.println("REMOVE ArrayList: "+PerfomanceMonitor.getPerfomance(()->arrayList.remove(9999)));
        System.out.println("REMOVE LinkedList: "+PerfomanceMonitor.getPerfomance(()->linkedList.remove(9999)));

        List<Integer> indexes=new ArrayList<>(10000);
        for(int i=arrayList.size()-1; i>=0; i--){
            indexes.add(i);
        }

        System.out.println("AVG GET ArrayList: "+PerfomanceMonitor.getAveragePerfomence(indexes, (i)->arrayList.get(i)));
        System.out.println("AVG GET LinkedList: "+PerfomanceMonitor.getAveragePerfomence(indexes, (i)->linkedList.get(i)));

        System.out.println("AVG REMOVE ArrayList: "+PerfomanceMonitor.getAveragePerfomence(indexes, (i)->arrayList.remove(i)));
        System.out.println("AVG REMOVE LinkedList: "+PerfomanceMonitor.getAveragePerfomence(indexes, (i)->linkedList.remove(i)));


    }

    public static void compareSet(){

        System.out.println("COMPARE SET");

        List<String> testData= Rand.getRandomStrings(10000);

        Set<String> hashSet=new HashSet<>();
        Set<String> linkedHashSet=new LinkedHashSet<>();
        Set<String> treeSet=new TreeSet<>();

        System.out.println("AVG ADD HashSet: "+PerfomanceMonitor.getAveragePerfomence(testData, (str)->hashSet.add(str)));
        System.out.println("AVG ADD LinkedSet: "+PerfomanceMonitor.getAveragePerfomence(testData, (str)->linkedHashSet.add(str)));
        System.out.println("AVG ADD TreeSet: "+PerfomanceMonitor.getAveragePerfomence(testData, (str)->treeSet.add(str)));

        Collections.shuffle(testData);

        System.out.println("AVG FIND HashSet: "+PerfomanceMonitor.getAveragePerfomence(testData, (str)->hashSet.contains(str)));
        System.out.println("AVG FIND LinkedSet: "+PerfomanceMonitor.getAveragePerfomence(testData, (str)->linkedHashSet.contains(str)));
        System.out.println("AVG FIND TreeSet: "+PerfomanceMonitor.getAveragePerfomence(testData, (str)->treeSet.contains(str)));

        System.out.println("AVG REMOVE HashSet: "+PerfomanceMonitor.getAveragePerfomence(testData, (str)->hashSet.remove(str)));
        System.out.println("AVG REMOVE LinkedSet: "+PerfomanceMonitor.getAveragePerfomence(testData, (str)->linkedHashSet.remove(str)));
        System.out.println("AVG REMOVE TreeSet: "+PerfomanceMonitor.getAveragePerfomence(testData, (str)->treeSet.remove(str)));

    }

    public static void compareMap(){
        System.out.println("COMPARE MAP");

        List<String> testData= Rand.getRandomStrings(10000);

        Map<String, String> hashMap=new HashMap<>();
        Map<String, String> linkedMap=new LinkedHashMap<>();
        Map<String, String> treeMap=new TreeMap<>();

        System.out.println("AVG ADD HashMap: "+PerfomanceMonitor.getAveragePerfomence(testData, (str)->hashMap.put(str, str)));
        System.out.println("AVG ADD LinkedMap: "+PerfomanceMonitor.getAveragePerfomence(testData, (str)->linkedMap.put(str, str)));
        System.out.println("AVG ADD TreeMap: "+PerfomanceMonitor.getAveragePerfomence(testData, (str)->treeMap.put(str, str)));

        System.out.println("AVG FIND HashMap: "+PerfomanceMonitor.getAveragePerfomence(testData, (str)->hashMap.get(str)));
        System.out.println("AVG FIND LinkedMap: "+PerfomanceMonitor.getAveragePerfomence(testData, (str)->linkedMap.get(str)));
        System.out.println("AVG FIND TreeMap: "+PerfomanceMonitor.getAveragePerfomence(testData, (str)->treeMap.get(str)));

        System.out.println("AVG REMOVE HashMap: "+PerfomanceMonitor.getAveragePerfomence(testData, (str)->hashMap.remove(str)));
        System.out.println("AVG REMOVE LinkedMap: "+PerfomanceMonitor.getAveragePerfomence(testData, (str)->linkedMap.remove(str)));
        System.out.println("AVG REMOVE TreeMap: "+PerfomanceMonitor.getAveragePerfomence(testData, (str)->treeMap.remove(str)));

    }

    static void testSieveEraposphene(){
        System.out.println("sieveEratosphene("+30+")="+ MyMath.sieveOfEratosphene(30));
    }

    static void testSwap(){
        List<Integer> arrayList=new ArrayList<>(15);
        List<Integer> linkedList=new LinkedList<>();
        for(int i=0; i<15; i++){
            arrayList.add(i);
            linkedList.add(i);
        }

        MyUtilCollections.swap(arrayList, 0, 10);
        MyUtilCollections.swap(linkedList, 0, 10);
        System.out.println(arrayList);
        System.out.println(linkedList);
    }

    static void testEx7_6(){
        Map<String, Set<Integer>> map=new HashMap<>();
        Set<Integer> set=new HashSet<>();
        set.add(1);
        map.put("1", set);
        System.out.println(MyUtilCollections.countAllIntegerValues(map));
    }

    static void testReadFiles(){
        TextFileStats stats=new TextFileStats("./res/voyna-i-mir-tom-1.txt");
        stats.readStatistics();
        stats.printWordsLineOccured("./out/voyna-i-mir-tom-1.stats-words-line-occured.txt");
        stats.printWordsOccured("./out/voyna-i-mir-tom-1.stats-words-occured.txt");
        stats.printOnlyLettersWords("./out/voyna-i-mir-tom-1.stats-words-only-letters.txt", 100);
        stats.printMostFrequenced("./out/voyna-i-mir-tom-1.stats-most-sequenced.txt", 10);
        stats.printLongestWords("./out/voyna-i-mir-tom-1.stats-longest-words.txt", 500);
        System.out.println("avg word length: "+stats.avgWordLength());
        System.out.println("max word length: "+stats.maxWords());
        System.out.println("stats saved to out direectory");
    }

    static void testDijkstra(){
        Graph graph=new Graph();
        graph.addNeighbor("1", new Neighbor("2", 7));
        graph.addNeighbor("1", new Neighbor("3", 9));
        graph.addNeighbor("1", new Neighbor("6", 14));
        graph.addNeighbor("2", new Neighbor("1", 7));
        graph.addNeighbor("2", new Neighbor("3", 10));
        graph.addNeighbor("2", new Neighbor("4", 15));
        graph.addNeighbor("3", new Neighbor("1", 9));
        graph.addNeighbor("3", new Neighbor("2", 10));
        graph.addNeighbor("3", new Neighbor("4", 11));
        graph.addNeighbor("3", new Neighbor("6", 2));
        graph.addNeighbor("4", new Neighbor("2", 15));
        graph.addNeighbor("4", new Neighbor("3", 11));
        graph.addNeighbor("4", new Neighbor("5", 6));
        graph.addNeighbor("5", new Neighbor("4", 6));
        graph.addNeighbor("5", new Neighbor("6", 9));
        graph.addNeighbor("6", new Neighbor("1", 14));
        graph.addNeighbor("6", new Neighbor("3", 2));
        graph.addNeighbor("6", new Neighbor("5", 9));
        System.out.println(graph.dijkstraAlgorithm("1"));
    }

    public static void testStream(){
        System.out.println(MyMath.avg(DoubleStream.of(1,2,3,4,5)));
    }
}
