package ru.pavlovka.homework5.util;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public interface Rand {
    static List<String> getRandomStrings(int count){
        List<String> result=new ArrayList<>(count);
        while (result.size()<count){
            result.add(UUID.randomUUID().toString());
        }
        return result;
    }
}
