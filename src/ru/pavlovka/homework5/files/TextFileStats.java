package ru.pavlovka.homework5.files;

import com.sun.org.apache.xerces.internal.xs.StringList;
import com.sun.xml.internal.ws.server.provider.ProviderInvokerTube;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class TextFileStats {

    private File textFile;

    private Map<String, Integer> wordsOccured;

    private Map<String, Set<Integer>> wordsLinesOccured;

    private Set<String> wordsOnlyLetters;

    public TextFileStats(String path){
        this(new File(path));
    }

    public TextFileStats(File file){
        textFile=file;
        wordsOccured=new TreeMap<>();
        wordsLinesOccured=new TreeMap<>();
        wordsOnlyLetters=new LinkedHashSet<>();
    }

    public void readStatistics(){
        wordsOccured.clear();
        wordsLinesOccured.clear();

        try {
            List<String> lines = Files.lines(textFile.toPath()).collect(Collectors.toList());
            int lineNumber=1;
            for(String line: lines){
                parseLine(line, lineNumber);
                lineNumber++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        };
    }

    public Map<String, Integer> getWordsOccured() {
        return wordsOccured;
    }

    public Map<String, Set<Integer>> getWordsLinesOccured() {
        return wordsLinesOccured;
    }

    public File getTextFile() {
        return textFile;
    }

    public void setTextFile(File textFile) {
        this.textFile = textFile;
    }

    private void parseLine(String line, int lineNumber){
        String[] words=line.split(" ");
        for(String word: words){
            if(olnlyLetters(word)){
                if(!wordsOnlyLetters.contains(word)){
                    wordsOnlyLetters.add(word);
                }
            }
            String cleanedWord=clear(word);
            int count=wordsOccured.getOrDefault(cleanedWord, 0);
            wordsOccured.put(cleanedWord, count+1);
            Set<Integer> set=wordsLinesOccured.getOrDefault(cleanedWord, new HashSet<>());
            set.add(lineNumber);
            wordsLinesOccured.put(cleanedWord, set);
        }
    }

    public void printWordsOccured(PrintWriter printWriter){
        for(Map.Entry<String, Integer> entry: wordsOccured.entrySet()){
            printWriter.println(entry.getKey()+" : "+entry.getValue());
        }
    }

    public void printWordsOccured(String filePath){
        try {
            printWordsOccured(new PrintWriter(filePath));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void printWordsLineOccured(PrintWriter printWriter){
        for(Map.Entry<String, Set<Integer>> entry: wordsLinesOccured.entrySet()){
            printWriter.println(entry.getKey()+" : "+entry.getValue());
        }
        printWriter.close();
    }

    public void printWordsLineOccured(String filePath){
        try {
            printWordsLineOccured(new PrintWriter(filePath));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void printMostFrequenced(String filePath, int count){
        try {
            printMostFrequenced(new PrintWriter(filePath), count);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void printMostFrequenced(PrintWriter printWriter, int count){
        for(String word: getMostFrequenced(count)){
            printWriter.println(word);
        }
        printWriter.close();
    }

    public List<String> getMostFrequenced(int count){
        List<String> result= wordsOccured
                .entrySet().stream()
                .sorted((o1, o2) -> -Integer.compare(o1.getValue(), o2.getValue()))
                .map(entry->entry.getKey()).limit(count).collect(Collectors.toList());
        return result;
    }

    public double avgWordLength(){
        int sum = wordsOccured
                .keySet()
                .stream()
                .map(word->word.length())
                .reduce(0, (left, right)->left+right);
        return (double)sum/wordsOccured.size();
    }

    public List<String> maxWords(){
        int max=wordsOccured.keySet().parallelStream()
                .map(word->word.length())
                .max(Integer::compareTo)
                .get();
        return wordsOccured.keySet()
                .stream().filter(word->word.length()==max)
                .collect(Collectors.toList());
    }

    public List<String> longestWords(int count){
        List<String> result= wordsOccured
                .keySet().parallelStream()
                .sorted((word1, word2)->-Integer.compare(word1.length(), word2.length()))
                .limit(count)
                .collect(Collectors.toList());
        return result;
    }

    public void printOnlyLettersWords(PrintWriter printWriter, int wordsCount){
        if(wordsCount==0){
            wordsCount=wordsOnlyLetters.size();
        }

        int i=0;
        for(String word: wordsOnlyLetters){
            printWriter.println(word);
            i++;
            if(i>=wordsCount){
                printWriter.println(word);
            }
        }
        printWriter.close();
    }

    public void printOnlyLettersWords(String filePath, int wordsCount){
        try {
            printOnlyLettersWords(new PrintWriter(filePath), wordsCount);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void printLongestWords(String filePath, int wordsCount){
        try {
            printLongestWords(new PrintWriter(filePath), wordsCount);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void printLongestWords(PrintWriter printWriter, int wordsCount){
        for(String word: longestWords(wordsCount)){
            printWriter.println(word);
        }
        printWriter.close();
    }

    private String clear(String rawWord){
        return rawWord.trim().replaceAll("[^\\P{P}-]+", "").toLowerCase();
        //return rawWord.trim().replaceAll("\\p{Punct}", "").toLowerCase();
    }

    private boolean olnlyLetters(String word){
        return (word.codePoints().filter(codePoint->Character.isAlphabetic(codePoint)).count()==word.length());
    }
}
