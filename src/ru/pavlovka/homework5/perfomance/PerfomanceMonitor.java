package ru.pavlovka.homework5.perfomance;

import java.util.Collection;
import java.util.function.Consumer;
import java.util.function.Function;

public interface PerfomanceMonitor {
    static long getPerfomance(MeasurableAction action) {
        long before = System.nanoTime();
        action.execute();
        long after = System.nanoTime();
        return after - before;
    }

    static <T> double getAveragePerfomence(Collection<T> data, Consumer<T> function){
        long result=0;
        for(T element: data) {
            result += getPerfomance(() -> function.accept(element));
        }
        return (double)result/data.size();
    }
}
