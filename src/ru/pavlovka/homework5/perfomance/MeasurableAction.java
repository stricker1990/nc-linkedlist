package ru.pavlovka.homework5.perfomance;

@FunctionalInterface
public interface MeasurableAction {
    void execute();
}
