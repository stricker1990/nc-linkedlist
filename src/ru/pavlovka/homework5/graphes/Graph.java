package ru.pavlovka.homework5.graphes;

import java.util.*;

public class Graph {
    private Map<String, Set<Neighbor>> network;

    public Graph(){
        network=new HashMap<>();
    }

    public Map<String, Integer> dijkstraAlgorithm(String startCity) {

        Map<String, Integer> result=new HashMap<>();

        PriorityQueue<Neighbor> queue=new PriorityQueue<>();
        queue.add(new Neighbor(startCity, 0));

        queue.addAll(network.get(startCity));

        while (!queue.isEmpty()){
            Neighbor u=queue.poll();

            result.put(u.getName(), u.getDistance());

            String city=u.getName();

            Set<Neighbor> neighbors=network.get(city);
            for(Neighbor neighbor: neighbors){
                String nextCity=neighbor.getName();
                int distance=neighbor.getDistance();
                int distanceThroughU=u.getDistance()+distance;
                if(distanceThroughU<neighbor.getDistance()){
                    queue.remove(neighbor);
                    queue.add(new Neighbor(nextCity, distanceThroughU));
                }
            }
        }

        return result;
    }

    public void clear(){
        network.clear();
    }

    public void addNeighbor(String city, Neighbor neighbor){
        Set<Neighbor> neighbors=network.get(city);
        if(neighbors==null){
            neighbors=new HashSet<>();
            network.put(city, neighbors);
        }
        neighbors.add(neighbor);
    }

    public Map<String, Set<Neighbor>> getNetwork() {
        return network;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Graph graph = (Graph) o;
        return Objects.equals(network, graph.network);
    }

    @Override
    public int hashCode() {
        return Objects.hash(network);
    }

    @Override
    public String toString() {
        return "Graph{" +
                "network=" + network +
                '}';
    }
}
