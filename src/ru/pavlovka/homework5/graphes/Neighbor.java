package ru.pavlovka.homework5.graphes;

import java.util.Objects;

public class Neighbor implements Comparable<Neighbor> {
    private String name;
    private int distance;

    public Neighbor(String name, int distance) {
        this.name = name;
        this.distance = distance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    @Override
    public String toString() {
        return "Neighbor{" +
                "name='" + name + '\'' +
                ", distance=" + distance +
                '}';
    }

    @Override
    public int compareTo(Neighbor o) {
        if(distance<o.distance){
            return -1;
        }else if(distance>o.distance){
            return 1;
        }
        return 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Neighbor neighbor = (Neighbor) o;
        return distance == neighbor.distance &&
                Objects.equals(name, neighbor.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, distance);
    }
}
